"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.07.08
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: Testing script
"""

import argparse
import sys
import os
import time

from easydict import EasyDict as edict
import cv2
import torch
import numpy as np

sys.path.append('../')

import config.kitti_config as cnf
from data_process import kitti_data_utils, kitti_bev_utils
from data_process.kitti_dataloader import create_test_dataloader
from models.model_utils import create_model
from utils.misc import make_folder
from utils.evaluation_utils import post_processing, rescale_boxes, post_processing_v2
from utils.misc import time_synchronized
from utils.visualization_utils import show_image_with_boxes, merge_rgb_to_bev, predictions_to_kitti_format
import pickle
from calculate_IoU import find_gtruth

def parse_test_configs():
    parser = argparse.ArgumentParser(description='Demonstration config for Complex YOLO Implementation')
    parser.add_argument('--saved_fn', type=str, default='complexer_yolov4', metavar='FN',
                        help='The name using for saving logs, models,...')
    parser.add_argument('-a', '--arch', type=str, default='darknet', metavar='ARCH',
                        help='The name of the model architecture')
    parser.add_argument('--cfgfile', type=str, default='E:/experiment/yolo-lidar/Complex-YOLOv4-lidar/src/config/cfg/complex_yolov4.cfg', metavar='PATH',###
                        help='The path for cfgfile (only for darknet)')
    parser.add_argument('--pretrained_path', type=str, default="E:/experiment/yolo-lidar/Complex-YOLOv4-lidar/checkpoints/complex_yolov4/complex_yolov4_mse_loss.pth", metavar='PATH',##
                        help='the path of the pretrained checkpoint')
    parser.add_argument('--use_giou_loss', action='store_true',
                        help='If true, use GIoU loss during training. If false, use MSE loss for training')

    parser.add_argument('--no_cuda', action='store_true', default=False,
                        help='If true, cuda is not used.')
    parser.add_argument('--gpu_idx', default=0, type=int,
                        help='GPU index to use.')

    parser.add_argument('--img_size', type=int, default=608,
                        help='the size of input image')
    parser.add_argument('--num_samples', type=int, default=None,
                        help='Take a subset of the dataset to run and debug')
    parser.add_argument('--num_workers', type=int, default=1,
                        help='Number of threads for loading data')
    parser.add_argument('--batch_size', type=int, default=1,
                        help='mini-batch size (default: 4)')

    parser.add_argument('--conf_thresh', type=float, default=0.5,
                        help='the threshold for conf')
    parser.add_argument('--nms_thresh', type=float, default=0.5,
                        help='the threshold for conf')

    parser.add_argument('--show_image', action='store_true', default=True,
                        help='If true, show the image during demostration')
    parser.add_argument('--save_test_output', action='store_true',
                        help='If true, the output image of the testing phase will be saved')
    parser.add_argument('--output_format', type=str, default='image', metavar='PATH',
                        help='the type of the test output (support image or video)')
    parser.add_argument('--output_video_fn', type=str, default='out_complexer_yolov4', metavar='PATH',
                        help='the video filename if the output format is video')

    configs = edict(vars(parser.parse_args()))
    configs.pin_memory = True

    ####################################################################
    ##############Dataset, Checkpoints, and results dir configs#########
    ####################################################################
    configs.working_dir = '../'
    configs.dataset_dir = os.path.join(configs.working_dir, 'dataset', 'kitti')

    if configs.save_test_output:
        configs.results_dir = os.path.join(configs.working_dir, 'results', configs.saved_fn)
        make_folder(configs.results_dir)

    return configs


if __name__ == '__main__':
    configs = parse_test_configs()
    configs.distributed = False  # For testing

    model = create_model(configs)
    model.print_network()
    print('\n\n' + '-*=' * 30 + '\n\n')
    
    device_string = 'cpu' if configs.no_cuda else 'cuda:{}'.format(configs.gpu_idx)
    
    assert os.path.isfile(configs.pretrained_path), "No file at {}".format(configs.pretrained_path)
    model.load_state_dict(torch.load(configs.pretrained_path, map_location=device_string))

    configs.device = torch.device(device_string)
    model = model.to(device=configs.device)

    out_cap = None

    model.eval()

    test_dataloader = create_test_dataloader(configs)
    # f = open('ground_truth1.pkl', 'rb')
    # ground_tr = pickle.load(f)
    # f.close()
    ground_tr = None
    ground_tr_back = None
    x_g = 12.0
    y_g = 4.0
    rot = 1.0
    if x_g < 0:
        ground_tr_back = find_gtruth(x_g, y_g, rot)
    else: ground_tr = find_gtruth(x_g, y_g, rot)

    with torch.no_grad():
        for batch_idx, (img_paths, imgs_bev, img_back) in enumerate(test_dataloader):##
            corners_all = []
            input_imgs = imgs_bev.to(device=configs.device).float()
            input_imgs_back = img_back.to(device=configs.device).float()
            t1 = time_synchronized()
            outputs = model(input_imgs)
            outputs_back = model(input_imgs_back)
            t2 = time_synchronized()
            detections = post_processing_v2(outputs, conf_thresh=configs.conf_thresh, nms_thresh=configs.nms_thresh)
            detections_back = post_processing_v2(outputs_back, conf_thresh=configs.conf_thresh, nms_thresh=configs.nms_thresh)

            ###front####
            img_detections = []  # Stores detections for each image index
            img_detections.extend(detections)

            img_bev = imgs_bev.squeeze() * 255
            img_bev = img_bev.permute(1, 2, 0).numpy().astype(np.uint8)
            img_bev = cv2.resize(img_bev, (configs.img_size, configs.img_size))
            
            for detections in img_detections:
                if detections is None:
                    continue
                # Rescale boxes to original image
                detections = rescale_boxes(detections, configs.img_size, img_bev.shape[:2])
                for x, y, w, l, im, re, *_, cls_pred in detections:
                    yaw = np.arctan2(im, re)
                    # Draw rotated box
                    corners_int = kitti_bev_utils.drawRotatedBox(img_bev, x, y, w, l, yaw, cnf.colors[int(cls_pred)])
                    kitti_bev_utils.drawGroundT(img_bev, ground_tr=ground_tr)
                    corners_all.append(corners_int)
                    
            # draw ground truth boxes
               # kitti_bev_utils.drawGroundTruth
            ###front#### 

            ###back####
            img_detections_back = []  # Stores detections for each image index
            img_detections_back.extend(detections_back)

            img_back = img_back.squeeze() * 255
            img_back = img_back.permute(1, 2, 0).numpy().astype(np.uint8)
            img_back = cv2.resize(img_back, (configs.img_size, configs.img_size))
            for detections in img_detections_back:
                if detections is None:
                    continue
                # Rescale boxes to original image
                detections = rescale_boxes(detections, configs.img_size, img_back.shape[:2])
                for x, y, w, l, im, re, *_, cls_pred in detections:
                    yaw = np.arctan2(im, re)
                    # Draw rotated box
                    corners_int = kitti_bev_utils.drawRotatedBox(img_back, x, y, w, l, yaw, cnf.colors[int(cls_pred)],front=False)
                    kitti_bev_utils.drawGroundT(img_back, ground_tr=ground_tr_back)
                    corners_all.append(corners_int)
            ###back####

            # This is the object detection accuracy metric. Max is 1.0 for a single vehicle.
            if x_g <0:
                total_iou = kitti_bev_utils.getIoU(corners_all, ground_tr_back)
            else: total_iou = kitti_bev_utils.getIoU(corners_all, ground_tr)
            # f = open('ground_truth1.pkl', 'wb')
            # pickle.dump(corners_all, f)
            # f.close()
            # total_iou = kitti_bev_utils.getIoU(corners_all, ground_tr)
            print("Intersection over Union Score:  ", total_iou)
            img_bev = cv2.flip(cv2.flip(img_bev, 0), 1)
            img_back = cv2.flip(cv2.flip(img_back, 0), 1)

            out_img = merge_rgb_to_bev(img_bev, img_back, output_width=608)
            #out_img = merge_rgb_to_bev(img_rgb, img_rgb1, output_width=608)

            print('\tDone testing the {}th sample, time: {:.1f}ms, speed {:.2f}FPS'.format(batch_idx, (t2 - t1) * 1000,
                                                                                           1 / (t2 - t1)))

            if configs.save_test_output:
                if configs.output_format == 'image':
                    img_fn = os.path.basename(img_paths[0])[:-4]
                    cv2.imwrite(os.path.join(configs.results_dir, '{}.jpg'.format(img_fn)), out_img)
                elif configs.output_format == 'video':
                    if out_cap is None:
                        out_cap_h, out_cap_w = out_img.shape[:2]
                        fourcc = cv2.VideoWriter_fourcc(*'MJPG')
                        out_cap = cv2.VideoWriter(
                            os.path.join(configs.results_dir, '{}.avi'.format(configs.output_video_fn)),
                            fourcc, 30, (out_cap_w, out_cap_h))

                    out_cap.write(out_img)
                else:
                    raise TypeError

            if configs.show_image:
                cv2.imshow('test-img', out_img)
                print('\n[INFO] Press n to see the next sample >>> Press Esc to quit...\n')
                if cv2.waitKey(0) & 0xFF == 27:
                    break
    if out_cap:
        out_cap.release()
    cv2.destroyAllWindows()

def test_single_data(root, sample_name):
    # load 
    lidarData = np.load(os.path.join(root, sample_name))
    b = kitti_bev_utils.removePoints(lidarData, cnf.boundary)
    b_back = kitti_bev_utils.removePoints(lidarData, cnf.boundary_back)
    rgb_map = kitti_bev_utils.makeBVFeature(b, cnf.DISCRETIZATION, cnf.boundary)
    rgb_map_back = kitti_bev_utils.makeBVFeature(b_back, cnf.DISCRETIZATION_BACK, cnf.boundary_back)

    configs = parse_test_configs()
    configs.distributed = False  # For testing
    model = create_model(configs)
    device_string = 'cpu' if configs.no_cuda else 'cuda:{}'.format(configs.gpu_idx)
    assert os.path.isfile(configs.pretrained_path), "No file at {}".format(configs.pretrained_path)
    model.load_state_dict(torch.load(configs.pretrained_path, map_location=device_string))
    configs.device = torch.device(device_string)
    model = model.to(device=configs.device)
    model.eval()
    # f = open('E:\experiment\Complex-YOLOv4-lidar\src\ground_truth1.pkl', 'rb')
    # ground_tr = pickle.load(f)
    # f.close()
    ground_tr = None
    ground_tr_back = None
    x_g = -12.0
    y_g = 4.0
    if x_g < 0:
        ground_tr_back = find_gtruth(x_g, y_g)
    else: ground_tr = find_gtruth(x_g, y_g)
    
    with torch.no_grad():
        corners_all = []
        input_imgs = torch.from_numpy(rgb_map).to(device=configs.device).float()
        input_imgs = input_imgs[None, :]
        input_imgs_back = torch.from_numpy(rgb_map_back).to(device=configs.device).float()
        input_imgs_back = input_imgs_back[None, :]
        outputs = model(input_imgs)
        outputs_back = model(input_imgs_back)
        detections = post_processing_v2(outputs, conf_thresh=configs.conf_thresh, nms_thresh=configs.nms_thresh)
        detections_back = post_processing_v2(outputs_back, conf_thresh=configs.conf_thresh, nms_thresh=configs.nms_thresh)

        ###front####
        img_detections = []  # Stores detections for each image index
        img_detections.extend(detections)

        img_bev = torch.from_numpy(rgb_map).squeeze() * 255
        img_bev = img_bev.permute(1, 2, 0).numpy().astype(np.uint8)
        img_bev = cv2.resize(img_bev, (configs.img_size, configs.img_size))
        
        for detections in img_detections:
            if detections is None:
                continue
            # Rescale boxes to original image
            detections = rescale_boxes(detections, configs.img_size, img_bev.shape[:2])
            for x, y, w, l, im, re, *_, cls_pred in detections:
                yaw = np.arctan2(im, re)
                # Draw rotated box
                corners_int = kitti_bev_utils.drawRotatedBox(img_bev, x, y, w, l, yaw, cnf.colors[int(cls_pred)],ground_tr=ground_tr)
                corners_all.append(corners_int)
                
        # draw ground truth boxes
            # kitti_bev_utils.drawGroundTruth
        ###front#### 

        ###back####
        img_detections_back = []  # Stores detections for each image index
        img_detections_back.extend(detections_back)

        img_back = torch.from_numpy(rgb_map_back).squeeze() * 255
        img_back = img_back.permute(1, 2, 0).numpy().astype(np.uint8)
        img_back = cv2.resize(img_back, (configs.img_size, configs.img_size))
        for detections in img_detections_back:
            if detections is None:
                continue
            # Rescale boxes to original image
            detections = rescale_boxes(detections, configs.img_size, img_back.shape[:2])
            for x, y, w, l, im, re, *_, cls_pred in detections:
                yaw = np.arctan2(im, re)
                # Draw rotated box
                corners_int = kitti_bev_utils.drawRotatedBox(img_back, x, y, w, l, yaw, cnf.colors[int(cls_pred)],front=False,ground_tr=ground_tr_back)
                corners_all.append(corners_int)
        ###back####

        # This is the object detection accuracy metric. Max is 1.0 for a single vehicle.
        if x_g <0:
            total_iou = kitti_bev_utils.getIoU(corners_all, ground_tr_back)
        else: total_iou = kitti_bev_utils.getIoU(corners_all, ground_tr)
        #print("Intersection over Union Score:  ", total_iou)
       
    return total_iou