#func    
    #take lidar data 
    #apply fog
    #give it to yolo
    #retrun the score
#minimize func-threshold based on the fog params
import sys
from test import test_single_data
sys.path.append('../../LiDAR_fog_sim')
from add_fog import add_fog
#from benderopt import minimize
from skopt import gp_minimize
import numpy as np

root = 'optimization_pc'#r'D:\experiment\Complex-YOLOv4-lidar\dataset\kitti\testing\velodyne'

def objective_func(params):
    # beta_mor: beta*mor
    # first calculate the score for the original data
    #IoU_wo_fog = test_single_data(root, '000010.bin')
    # add fog and recalculate
    alpha = params[0]
    beta_mor = params[1]
    gamma = params[2]
    mor = np.log(20) / alpha
    add_fog(alpha, beta_mor/mor, gamma)
    IoU_with_fog = test_single_data(root, 'distorted_lidar.bin')
    return np.abs(3 - IoU_with_fog)
    #print("IoU without fog: ", IoU_wo_fog, "IoU with fog: ", IoU_with_fog)

if __name__ == '__main__':
    space = [(0.003, 0.5),(0.023, 0.092),(0.0000001, 0.00001)]
    res = gp_minimize(objective_func,    # the function to minimize
                    space,               # the bounds on each dimension of x
                    acq_func="EI",       # the acquisition function
                    n_calls=25,          # the number of evaluations of f
                    n_random_starts=5,   # the number of random initialization points
                    noise=0,             # the noise level (optional)
                    random_state=1234)   # the random seed
    print(res)
    # optimization_problem_parameters = [
    #     {
    #         "name": "x", 
    #         "category": "uniform",
    #         "search_space": {
    #             "low": 0,
    #             "high": 2 * np.pi,
    #         }
    #     }
    # ]

    # # We launch the optimization
    # best_sample = minimize(f, optimization_problem_parameters, number_of_evaluation=50)
