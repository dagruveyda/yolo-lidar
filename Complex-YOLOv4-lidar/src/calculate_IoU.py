import numpy as np
from shapely.geometry import Polygon
import pickle
import math 

car1 = np.array([[203.64264 , 127.818375], [199.76031, 69.59511], [224.37357,  67.9539 ], [228.25589, 126.17717]])
car2 = np.array([[270.54715, 451.1397 ], [270.89047, 514.45935], [243.59128, 514.60736], [243.24796, 451.28772]])
car3 = np.array([[325.71494, 186.75111], [327.33017, 131.54085], [350.77005, 132.22661], [349.15482, 187.43687]])
amb = np.array([[360.26434, 453.48685], [360.6411 , 518.99426], [333.30316, 519.1515 ], [332.9264 , 453.64407]])
ground_tr = [car1, car2, car3, amb]

test = [[[203.16736, 127.65338], [200.26746 ,  69.334595], [224.8935 ,  68.11008], [227.7934 , 126.42886]],
        [[325.60544, 186.95764], [327.14212, 131.63187], [350.61227, 132.28375], [349.0756 , 187.60953]],
        [[270.98077, 451.1605 ], [270.64526, 515.01135], [243.38063, 514.8681 ], [243.71611, 451.01724]],
        [[324.0966, 514.4999], [343.01468, 451.45523], [368.76962, 459.18362], [349.85153, 522.2283 ]]]

f = open('ground_truth.pkl', 'wb')
pickle.dump(ground_tr, f)
f.close()

# f = open('ground_truth.pkl', 'rb')
# a = pickle.load(f)
# f.close()

def find_gtruth(x, y, rot):
    # x:car front
    # lidar in world frame
    lidar_x = 2.25
    lidar_y = 8.0
    x_wrtlidar = x - lidar_x
    y_wrtlidar = y - lidar_y

    # corner w.r.t car frame
    x1 = corner1_x = -1.1175
    y1 = corner1_y = -1.125
    x2 = corner2_x = -1.1175
    y2 = corner2_y = 1.125
    x3 = corner3_x = 3.755
    y3 = corner3_y = -1.125
    x4 = corner4_x = 3.755
    y4 = corner4_y = 1.125
    # rot is wrt car frame, corners rotate wrt to that
    cos = math.cos(rot)
    sin = math.sin(rot)
    corner1_x = cos * x1 - sin * y1
    corner1_y = sin * x1 + cos * y1
    corner2_x = cos * x2 - sin* y2
    corner2_y = sin * x2 + cos * y2
    corner3_x = cos * x3 - sin * y3
    corner3_y = sin * x3 + cos * y3
    corner4_x = cos * x4 - sin * y4
    corner4_y = sin * x4 + cos * y4

    # corners wrt. lidar frame and scale the corners
    corner1_x += x_wrtlidar
    corner1_x *= 12.0
    #math.sin(rot)*
    corner1_y += y_wrtlidar
    corner1_y *= 12.0
    # x1 = corner1_x
    # y1 = corner1_y
    # # rotate here
    # corner1_x = math.cos(rot) * x1 - math.sin(rot) * y1
    # corner1_y = math.sin(rot) * x1 - math.cos(rot) * y1
    corner1_y += 304

    corner2_x += x_wrtlidar
    corner2_x *= 12.0
    corner2_y += y_wrtlidar
    corner2_y *= 12.0
    # x2 = corner2_x
    # y2 = corner2_y
    # # rotate here
    # corner2_x = math.cos(rot) * x2 - math.sin(rot) * y2
    # corner2_y = math.sin(rot) * x2 - math.cos(rot) * y2
    corner2_y += 304

    corner3_x += x_wrtlidar
    corner3_x *= 12.0
    corner3_y += y_wrtlidar
    corner3_y *= 12.0
    corner3_y += 304
    corner4_x += x_wrtlidar
    corner4_x *= 12.0
    corner4_y += y_wrtlidar
    corner4_y *= 12.0
    corner4_y += 304
    corners = np.array([[corner2_y, corner2_x], [corner1_y , corner1_x], [corner3_y,  corner3_x], [corner4_y, corner4_x]]).astype(int)
    # if the car is behind the ego, put it into the lower frame
    if x < 0:
        corners[:,1] += 608
    return [corners]

def calculate(detections):
    iou_final = 0
    for detection in detections:
        iou = []
        for car in ground_tr:
            poly_1 = Polygon(detection)
            poly_2 = Polygon(car)
            iou.append(poly_1.intersection(poly_2).area / poly_1.union(poly_2).area)
        iou_final += max(iou)
    return iou_final

print(calculate(test))