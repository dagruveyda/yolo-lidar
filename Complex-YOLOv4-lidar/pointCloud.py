import pyviz3d.visualizer as viz
import numpy as np

def visualize_pc(coordinates, file_name):
  visualizer = viz.Visualizer()
  point_size = 35
  #coordinates = coordinates.detach().cpu().numpy()
  coordinates = coordinates[:, :3] - coordinates[:, :3].mean(axis=0)
  coordinates = coordinates * 0.05
  colors = np.random.randint(256, size=(len(coordinates), 3))
  visualizer.add_points(
      name="original_pc",
      positions=coordinates,
      point_size=point_size,
      colors=colors,
      visible=True,
  )
  file_name = "E:/experiment/Complex-YOLOv4-lidar/visualizations/" + file_name
  visualizer.save(file_name, verbose=False)

if __name__ == '__main__':
    coordinates = np.load(r'E:\experiment\Complex-YOLOv4-lidar\dataset\kitti\testing\velodyne\000000.bin')
    file_name = "point_cloud"
    visualize_pc(coordinates, file_name)